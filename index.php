<?php
include 'header.php';
?>

	
	<section class="intro-section">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-lg-2"></div>
				<div class="col-md-10 col-lg-8">
					<div class="intro">
						<div class="profile-img"><img src="./images/my.jpg" alt=""></div>
						<h2><b>Muzaffar Munir</b></h2>
						<h4 class="font-yellow">Software Engineer</h4>
						<ul class="information margin-tb-30">
							
							<li><b>EMAIL : </b>muzafarmunir@gmail.com</li>
							
						</ul>
						<ul class="social-icons">
							
							<li><a href="https://www.linkedin.com/in/muzaffar-munir-300060110?trk=nav_responsive_tab_profile_pic"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="https://www.instagram.com/muzaffarmunir90"><i class="fa fa-instagram"></i></a></li>
							<li><a href="https://www.facebook.com/muzaffarmunir90"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/MuzaffarMunir90"><i class="fa fa-twitter"></i></a></li>
						</ul>
					</div><!-- intro -->
				</div><!-- col-sm-8 -->
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- intro-section -->
	
	<section class="portfolio-section section">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="heading">
						<h3><b>Portfolio</b></h3>
						<h6 class="font-lite-black"><b>MY WORK</b></h6>
					</div>
				</div><!-- col-sm-4 -->
				<div class="col-sm-8">
					<div class="portfolioFilter clearfix margin-b-80">
						 <a href="#" data-filter="*" class="current"><b>ALL</b></a>
						<a href="#" data-filter=".web-design"><b>WEB Development</b></a>
						<a href="#" data-filter=".crm"><b>CRM Development</b></a>
						<!--<a href="#" data-filter=".graphic-design"><b>Free Lancing</b></a> -->
					</div><!-- portfolioFilter -->
				</div><!-- col-sm-8 -->
			</div><!-- row -->
		</div><!-- container -->
		
		<div class="portfolioContainer">
			
			<div class="p-item web-design">
				<a href="https://binimarina.com/" target="_blank">
				<img src="./images/experiences/binimarina.jpg" alt="">
				</a>
			</div><!-- p-item -->
			<div class="p-item web-design">
				<a href="http://winhud.com/" target="_blank">
					<img src="/images/experiences/winhud.jpg" alt="">
					</a>
			</div><!-- p-item -->
			
			
			<div class="p-item web-design">
				<a href="http://elisaproperties.com/" target="_blank">
					<img src="./images/experiences/EP.jpg" alt=""></a>
			</div><!-- p-item-->
			
			<div class="p-item web-design">
				<a href="http://mod4.optima-webs.com" target="_blank">
					<img src="./images/experiences/mod4.jpg" alt=""></a>
			</div><!-- p-item -->
			
			

			<div class="p-item  web-design">
				<a href="http://ibizamyproperty.com/" target="_blank">
					<img src="./images/experiences/IMP.jpg" alt=""></a>
			</div><!-- p-item -->
			
			 <div class="p-item  we-design crm">
				<a href="https://my.optima-crm.com/" target="_blank">
					 <img src="./images/experiences/v1.jpg" alt=""></a>
			</div><!-- p-item --> 
			<div class="p-item  we-design crm">
				<a href="https://my2.optima-crm.com/" target="_blank">
					 <img src="./images/experiences/CRMv2.jpg" alt=""></a>
			</div><!-- p-item --> 
				
			<!-- <div class="p-item web-design crm">
				<a href="images/portfolio-6-600x800.jpg" data-fluidbox>
					<!-- <img src="images/portfolio-6-600x800.jpg" alt=""></a>
			</div> --> <!-- p-item -->

			<!-- <div class="p-item p-item-2 graphic-design">
				<a class="img" href="images/kiddie/4.png" data-fluidbox>
					<img src="images/kiddie/4.png" alt=""></a>
				<a class="img" href="images/portfolio-11-300x400.jpg" data-fluidbox>
					<img src="images/portfolio-11-300x400.jpg" alt=""></a>
			</div> --><!-- p-item -->
		
		</div><!-- portfolioContainer -->
		
	</section><!-- portfolio-section -->
	
	
	<section class="about-section section">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="heading">
						<h3><b>About me</b></h3>
						<h6 class="font-lite-black"><b><!-- PROFESSIONAL PATH --></b></h6>
					</div>
				</div><!-- col-sm-4 -->
				<div class="col-sm-8">
					<p class="margin-b-50">i am a professional web developer and freelancer who have done many project and wants to joins an where there is healthy growth and competitive environment in which I will be able to participate in learning and improve my existing professional skills as well as learn new technologies and get a good grip on web development</p>
					
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="radial-prog-area margin-b-30">
								<div class="radial-progress" data-prog-percent=".95">
									<div></div>
									<h6 class="progress-title">HTML & CSS</h6>
								</div>
							</div><!-- radial-prog-area-->
						</div><!-- col-sm-6-->
					
						<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="radial-prog-area margin-b-30">
								<div class="radial-progress" data-prog-percent=".85">
									<div></div>
									<h6 class="progress-title">BootStrap</h6>
								</div>
							</div><!-- radial-prog-area-->
						</div><!-- col-sm-6-->
						
						<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="radial-prog-area margin-b-30">
								<div class="radial-progress" data-prog-percent=".80">
									<div></div>
									<h6 class="progress-title">Databases</h6>
								</div>
							</div><!-- radial-prog-area-->
						</div><!-- col-sm-6-->
							<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="radial-prog-area margin-b-30">
								<div class="radial-progress" data-prog-percent=".70">
									<div></div>
									<h6 class="progress-title">MongoDb</h6>
								</div>
							</div><!-- radial-prog-area-->
						</div><!-- col-sm-6-->
						
						<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="radial-prog-area margin-b-50">
								<div class="radial-progress" data-prog-percent=".70">
									<div></div>
									<h6 class="progress-title">Jquery & AJAX</h6>
								</div>
							</div><!-- radial-prog-area-->
						</div><!-- col-sm-6-->
					
					<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="radial-prog-area margin-b-50">
								<div class="radial-progress" data-prog-percent=".80">
									<div></div>
									<h6 class="progress-title">PHP</h6>
								</div>
							</div><!-- radial-prog-area-->
						</div><!-- col-sm-6-->

						<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="radial-prog-area margin-b-50">
								<div class="radial-progress" data-prog-percent=".60">
									<div></div>
									<h6 class="progress-title">Node Js & Angular</h6>
								</div>
							</div><!-- radial-prog-area-->
						</div><!-- col-sm-6-->



					</div><!-- row -->
				</div><!-- col-sm-8 -->
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- about-section -->
	
	<section class="experience-section section">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="heading">
						<h3><b>Work Experience</b></h3>
						<h6 class="font-lite-black"><b>PREVIOUS JOBS</b></h6>
					</div>
				</div><!-- col-sm-4 -->
				<div class="col-sm-8">
				
					<div class="experience margin-b-50">
						<h4><b>JUNIOR Software Engineer</b></h4>
						<h5 class="font-yellow"><b>Netstech </b>Gujrat</h5>
						<h6 class="margin-t-10">January 2018 - July 2018</h6>
						<p class="font-semi-white margin-tb-30">
Here my responsibilities include design and develop applications including define major attributes and designing database for the project
 </p>
						<ul class="list margin-b-30">
							<li>E-Commerce</li>
							<li>Video Downloading</li>
							<li>School Management</li>
							<li>Portfolio websites</li>
						</ul>
					</div><!-- experience -->
					
					<div class="experience margin-b-50">
						<h4><b>Junior Software Engineer</b></h4>
						<h5 class="font-yellow"><b>OptimaGeeks</b> Lahore</h5>
						<h6 class="margin-t-10">August 2018 -present</h6>
						<p class="font-semi-white margin-tb-30">•	Here I am working on PHP(Yii2), Asp.net MVC and, Asp.net Web Apis also working on Optima CRM</p>
						<ul class="list margin-b-30">
							<!-- <li>Duis non volutpat arcu, eu mollis tellus.</li>
							<li>Quis consequat nulla effi citur at.</li>
							<li>Sed finibus aliquam neque sit.</li> -->
						</ul>
					</div> <!-- experience -->
					
				</div><!-- col-sm-8 -->
			</div><!-- row -->
		</div><!-- container -->
		
	</section><!-- experience-section -->
	
	<section class="education-section section">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="heading">
						<h3><b>Education</b></h3>
						<h6 class="font-lite-black"><b>ACADEMIC CAREER</b></h6>
					</div>
				</div><!-- col-sm-4 -->
				<div class="col-sm-8">
					<div class="education-wrapper">
						<div class="education margin-b-50">
							<h4><b>BS Information Technology</b></h4>
							<h5 class="font-yellow"><b>University of Gujrat(UOG)</b></h5>
							<h6 class="font-lite-black margin-t-10">GRADUATED IN July 2018</h6>
							<p class="margin-tb-30">I am a individual who have enrolled in department of Information Technology in University of Gujrat from 2014-2018.During the session i have learned how to code and how to tackle different problems in Computer </p>
						</div><!-- education -->
						
						<!-- <div class="education margin-b-50">
							<h4><b>Int</b></h4>
							<h5 class="font-yellow"><b>NEW YORK PUBLIC UNIVERSITY</b></h5>
							<h6 class="font-lite-black margin-t-10">GRADUATED IN MAY 2009(6 MONTHS)</h6>
							<p class="margin-tb-30">Duis non volutpat arcu, eu mollis tellus. Sed finibus aliquam neque sit amet sodales. 
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla maximus pellentes que velit, 
							quis consequat nulla effi citur at. Maecenas sed massa tristique.Duis non volutpat arcu, 
							eu mollis tellus. Sed finibus aliquam neque sit amet sodales. </p>
						</div> --><!-- education -->
						
						<!-- <div class="education margin-b-50">
							<h4><b>GRADUATED VALEDICTERIAN</b></h4>
							<h5 class="font-yellow"><b>PUBLIC COLLEGE</b></h5>
							<h6 class="font-lite-black margin-t-10">GRADUATED IN MAY 2008(4 YEARS)</h6>
							<p class="margin-tb-30">Duis non volutpat arcu, eu mollis tellus. Sed finibus aliquam neque sit amet sodales. 
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla maximus pellentes que velit, 
							quis consequat nulla effi citur at. Maecenas sed massa tristique.Duis non volutpat arcu, 
							eu mollis tellus. Sed finibus aliquam neque sit amet sodales. </p>
						</div> --><!-- education -->
					</div><!-- education-wrapper -->
				</div><!-- col-sm-8 -->
			</div><!-- row -->
		</div><!-- container -->
		
	</section><!-- about-section -->
	
	<section class="counter-section" id="counter">
		<div class="container">
			<div class="row">
			
				
				<div class="col-sm-6 col-md-6 col-lg-4">
					<div class="counter margin-b-30">
						<h1 class="title"><b><span class="counter-value" data-duration="1400" data-count="20">0</span></b></h1>
						<h5 class="desc"><b>Projects</b></h5>
					</div><!-- counter -->
				</div><!-- col-md-3-->
				
				<div class="col-sm-6 col-md-6 col-lg-4">
					<div class="counter margin-b-30">
						<h1 class="title"><b><span class="counter-value" data-duration="700" data-count="10">0</span></b></h1>
						<h5 class="desc"><b>Satisfied Clients</b></h5>
					</div><!-- counter -->
				</div><!-- col-md-3-->
				
				<div class="col-sm-6 col-md-6 col-lg-4">
					<div class="counter margin-b-30">
						<h1 class="title"><b><span class="counter-value" data-duration="2000" data-count="12">0</span></b></h1>
						<h5 class="desc"><b>Finished Projects</b></h5>
					</div><!-- margin-b-30 -->
				</div><!-- col-md-3-->
				
			</div><!-- row-->
		</div><!-- container-->
    </section><!-- counter-section-->
	
<?php
include 'footer.php';
?>